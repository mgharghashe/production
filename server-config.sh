#!/usr/bin/env bash
# https://gitlab.com/mgharghashe/production
#
# Copyright (c) 2019 Mgharghashe. Released under the MIT License.

set -o errexit
clear

declare -A color
color[green]="\e[32m"
color[default]="\e[39m"
color[yellow]="\e[93m"
color[red]="\e[31m"
color[blue]="\e[34m"

currentUser=`id`
if [[ ! ${currentUser} =~ "root" ]] ; then
    echo 'please run this script with root access'
    exit 1
fi

isAptUpdate=0
installCurrentApp=0
domain=""

echo -e "${color[green]}welcome to config linux server for java base web application!!!"
echo -e "${color[default]}please tell me which application do you need to be install?"
echo  "***(notice: installer stop when script have error)***"

is_install(){
    echo -e "do you need to install ${color[blue]}$1${color[default]}?(y|n)"
    read install

    if [[ ${install} == "y" ]] || [[ ${install} == "Y" ]] || [[ ${install} == "yes" ]] ; then
        installCurrentApp=$((1))
    else
        installCurrentApp=$((0))
    fi
}

is_config(){
    echo -e "do you need to config ${color[blue]}$1${color[default]}?(y|n)"
    read install

    if [[ ${install} == "y" ]] || [[ ${install} == "Y" ]] || [[ ${install} == "yes" ]] ; then
        installCurrentApp=$((1))
    else
        installCurrentApp=$((0))
    fi
}

apt_get_update(){
    echo "update package..."
    if [[ ${isAptUpdate} -eq 0 ]] ; then
        if apt-get update ; then
            echo -e "${color[green]} update package successful${color[default]}"
        else
            echo -e "${color[red]} update package fail, but installer continue...${color[default]}"
         fi
    else
        isAptUpdate=$(($isAptUpdate + 1))
    fi
}

add_sub_domain(){
    echo -e "what is virtual host type for ${color[blue]}$1${color[default]}"
    echo "1)react web app"
    echo "2)java web app (proxy for tomcat)"
    echo "3)pure html"
    read webAppType

    echo -e "<VirtualHost *:80>\n"\
    "  ServerName $1.${domain}\n"\
    "  Redirect / https://$1.${domain}/\n"\
    "  ErrorLog \${APACHE_LOG_DIR}/$1.${domain}_error.log\n"\
    "  CustomLog \${APACHE_LOG_DIR}/$1.${domain}_access.log combined\n"\
    "</VirtualHost>" >> 000-default.conf

    echo -e "<VirtualHost *:443>\n"\
    "   ServerName $1.${domain}" >> 000-default.conf

    case ${webAppType} in
        1)
        if [[ $1 == "www" ]] ; then
            echo "  DocumentRoot /var/www/html" >> 000-default.conf
        else
            echo "  DocumentRoot /var/www/$1" >> 000-default.conf
            if  [[ ! -d /var/www/$1 ]]; then
                mkdir "/var/www/$1"
            fi
        fi

        echo -e "  RewriteEngine on\n"\
        "RewriteCond %{REQUEST_FILENAME} !-f\n"\
        "RewriteCond %{REQUEST_FILENAME} !-d\n"\
        "RewriteCond %{REQUEST_URI} !\.(?:html|TTF|css|eot|gif|ico|io|jpeg|jpg|js|json|ltr|map|md|php|png|svg|swo|swp|ttf|woff|woff2)$ [NC]\n"\
        "# otherwise forward it to index.html\n"\
        "RewriteRule ^/(.*) /index.html [NC,L]\n"\
        "\n"\
        "<Files index.html>\n"\
        "       FileETag None\n"\
        "        <ifModule mod_headers.c>\n"\
        "                Header unset ETag\n"\
        "                Header set Cache-Control \"max-age=0, no-cache, no-store, must-revalidate\"\n"\
        "                Header set Pragma \"no-cache\"\n"\
        "                Header set Expires \"Wed, 11 Jan 1984 05:00:00 GMT\"\n"\
        "        </ifModule>\n"\
        "</Files>\n"\
        "<IfModule mod_expires.c>\n"\
        "        ExpiresActive On\n"\
        "        ExpiresByType text/html \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/x-web-app-manifest+json \"access plus 0 seconds\"\n"\
        "        ExpiresByType text/cache-manifest \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/json \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/ld+json \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/schema+json \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/vnd.geo+json \"access plus 0 seconds\"\n"\
        "        ExpiresByType application/xml \"access plus 0 seconds\"\n"\
        "        ExpiresByType text/xml \"access plus 0 seconds\"\n"\
        "        ExpiresByType image/jpg \"access plus 1 year\"\n"\
        "        ExpiresByType image/jpeg \"access plus 1 year\"\n"\
        "        ExpiresByType image/gif \"access plus 1 year\"\n"\
        "        ExpiresByType image/png \"access plus 1 year\"\n"\
        "	    ExpiresByType application/font-woff \"access plus 1 year\"\n"\
        "        ExpiresByType text/css \"access plus 1 month\"\n"\
        "        ExpiresByType application/pdf \"access plus 1 month\"\n"\
        "        ExpiresByType text/x-javascript \"access plus 1 month\"\n"\
        "        ExpiresByType application/javascript \"access plus 1 month\"\n"\
        "        ExpiresByType application/x-shockwave-flash \"access plus 1 month\"\n"\
        "        ExpiresByType image/x-icon \"access plus 1 year\"\n"\
        "        ExpiresDefault \"access plus 2 days\"\n"\
        "</IfModule>\n" >> 000-default.conf

            ;;

        2)
        echo -e "  ProxyPreserveHost On\n"\
        "   ProxyPass \"/\" \"http://127.0.0.1:8080/\"\n"\
        "   ProxyPassReverse  \"/\" \"http://127.0.0.1:8080/\"\n\n"\
        "   RewriteEngine On\n"\
        "   RewriteCond %{HTTPS} !=on\n"\
        "   RewriteRule ^/?(.*) https://%{${domain}}/\$1 [R,L]" >> 000-default.conf
            ;;

        *)
        echo "add access rwx to this sub domain"
        if [[ $1 == "www" ]] ; then
            echo "  DocumentRoot /var/www/html" >> 000-default.conf
            setfacl  -R -d -m u::rwx "/var/www/html/"
            setfacl  -R -d -m g::rwx "/var/www/html/"
            setfacl  -R -d -m o::rwx "/var/www/html/"
        else
            echo "  DocumentRoot /var/www/$1" >> 000-default.conf
            if  [[ ! -d /var/www/$1 ]]; then
                mkdir "/var/www/$1"
            fi
            setfacl  -R -d -m u::rwx "/var/www/$1/"
            setfacl  -R -d -m g::rwx "/var/www/$1/"
            setfacl  -R -d -m o::rwx "/var/www/$1/"
        fi
        echo -e "   RewriteEngine On\n"\
        "   RewriteCond %{HTTPS} !=on\n"\
        "   RewriteRule ^/?(.*) https://%{${domain}}/\$1 [R,L]" >> 000-default.conf
        
        echo -e "   <IfModule mod_headers.c>\n"\
                "       Header set Access-Control-Allow-Origin \"*\"\n"\
                "   </IfModule>" >> 000-default.conf
            ;;
    esac

    echo -e "	SSLEngine on\n"\
	"   SSLCertificateFile	/etc/letsencrypt/live/${domain}/cert.pem\n"\
	"   SSLCertificateKeyFile /etc/letsencrypt/live/${domain}/privkey.pem\n"\
	"   SSLCACertificateFile /etc/letsencrypt/live/${domain}/fullchain.pem\n" >> 000-default.conf

    echo -e "  ErrorLog \${APACHE_LOG_DIR}/$1.${domain}_error.log\n"\
    "  CustomLog \${APACHE_LOG_DIR}/$1.${domain}_access.log combined\n" >> 000-default.conf
    echo -e "</VirtualHost>\n" >> 000-default.conf

    echo -e "$1 ${color[green]}added${color[default]}"
}

install_apache(){

    is_install apache
    if [[ ${installCurrentApp} -eq 1 ]] ; then
        apt_get_update
        echo "installing apache..."
        apt-get install apache2
        echo -e "apache install [${color[green]}done${color[default]}]"

        is_config "virtual host for apache (000-default.conf , default-ssl.conf will be override and ssl mode enable)"

        if [[ ${installCurrentApp} -eq 1 ]] ; then

            echo "please insert your domain without sub domain (e.g exmple.com) by default exmple.com redirected to www.exmple.com:"
            read domain

            cd /etc/apache2
            echo -e "Listen 80\nListen 443" > ports.conf

            cd /etc/apache2/sites-available
            if [[ -e 000-default.conf ]] ; then
                rm 000-default.conf
            fi

            if [[ -e default-ssl.conf ]] ; then
                rm default-ssl.conf
            fi

            echo "enable mods..."
            a2enmod ssl proxy proxy_html proxy_http rewrite headers
            echo -e "please add certificate file to ${color[blue]}/etc/letsencrypt/live/${domain}${color[default]}"
            echo -e "SSLCertificateFile --> cert.pem"
            echo -e "SSLCertificateKeyFile --> privkey.pem"
            echo -e "SSLCACertificateFile --> fullchain.pem"

            if  [[ ! -d /etc/cert/${domain} ]]; then
                mkdir -p "/etc/cert/${domain}"
            fi

            echo -e "<VirtualHost *:80>\n"\
            "  ServerName ${domain}\n"\
            "  Redirect / https://www.${domain}/\n"\
            "  ErrorLog \${APACHE_LOG_DIR}/${domain}_error.log\n"\
            "  CustomLog \${APACHE_LOG_DIR}/${domain}_access.log combined\n"\
            "</VirtualHost>" >> 000-default.conf

            add_sub_domain www

            apt-get install acl
            while true; do
                echo -e "please add next sub domain: '@end' for ending"
                read subDomain
                if [[ ${subDomain} == "@end" ]] ; then
                    break
                fi
                add_sub_domain ${subDomain}
            done

            if ! service apache2 restart; then
                echo -e "${color[red]}apache restart failed${color[default]}"
            fi
        fi
    fi
}

install_java() {
    is_install java

    if [[ ${installCurrentApp} -eq 1 ]] ; then
        cd /
        echo "please choose source to download java:"
        echo "1)https://repo.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz"
        echo "2)https://www.baloot-app.ir/jdk-8u221-linux-x64.tar.gz"
        echo  "3)enter url"
        echo  "4)skip"

        read chooseJava
        case ${chooseJava} in
        1)
            echo "downloading java..."
            wget https://repo.huaweicloud.com/java/jdk/8u202-b08/jdk-8u202-linux-x64.tar.gz
            echo "extract file..."
            tar -xvf jdk-8u202-linux-x64.tar.gz
            rm jdk-8u202-linux-x64.tar.gz
            ;;
        2)
            echo "downloading java..."
            wget https://www.baloot-app.ir/jdk-8u221-linux-x64.tar.gz --no-check-certificate
            echo "extract file..."
            tar -xvf jdk-8u221-linux-x64.tar.gz
            rm jdk-8u221-linux-x64.tar.gz
            ;;
        3)
            echo "inter url:"
            read javaUrl
            echo "downloading java..."
            wget ${javaUrl}
            echo "enter file name"
            read javaFile
            tar -xvf ${javaFile}
            rm ${javaFile}
            ;;
        *)
            echo "skip download"
        esac
        if [[ ! -e "/usr/lib/jvm/java-8" ]] ; then
            mkdir -p "/usr/lib/jvm/java-8"
        fi
        if  ! cd jdk*  ; then
            echo "enter jdk folder name:"
            read jdkFolderName
            cd ${jdkFolderName}
        fi
        pwd
        mv * /usr/lib/jvm/java-8
        update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/java-8/bin/java" 100
        update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/java-8/bin/javac" 100
        java -version
        javac -version
        echo -e "java install [${color[green]}done${color[default]}]"
    fi
}

install_tomcat(){
    is_install tomcat
    if [[ ${installCurrentApp} -eq 1 ]] ; then
        cd /
        wget https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.50/bin/apache-tomcat-8.5.50.tar.gz
        tar -xvf apache-tomcat-8.5.50.tar.gz
        rm apache-tomcat-8.5.50.tar.gz

        cd /apache-tomcat-8.5.50
        if [[ ! -e "/usr/local/tomcat" ]] ; then
            mkdir -p "/usr/local/tomcat"
        fi

        if ! mv * /usr/local/tomcat; then
            echo "tomcat directory not empty"
        fi

        cd /
        rm -r /apache-tomcat-8.5.50
        if useradd -r tomcat; then
            echo "tomcat user add"
        fi

        if mkdir app; then
            echo "create /app OK"
        fi
        chown tomcat:tomcat -R app/

        cd /usr/local
        chown tomcat:tomcat -R tomcat/

        cd tomcat/
        chmod 777 -R bin/

        sed -i 's/appBase="webapps"/appBase="\/app"/g' conf/server.xml

        echo "add tomcat to services"

        cd /
        echo -e "#!/bin/sh\n"\
            "\n"\
            "JAVA_HOME=\"/usr/lib/jvm/java-8\";\n"\
            "JRE_HOME=\"/usr/lib/jvm/java-8/jre\";\n"\
            "TOMCAT_USER=tomcat;\n"\
            "TOMCAT_PATH=\"/usr/local/tomcat\";\n"\
            "\n"\
            "start() {\n"\
            "        if [ -f \$TOMCAT_PATH/bin/startup.sh ];\n"\
            "            then\n"\
            "                echo -n \"Starting Tomcat\"\n"\
            "                su \$TOMCAT_USER -c \$TOMCAT_PATH/bin/startup.sh\n"\
            "        fi\n"\
            "}\n"\
            "\n"\
            "stop() {\n"\
            "        if [ -f \$TOMCAT_PATH/bin/shutdown.sh ];\n"\
            "            then\n"\
            "                echo -n \"Stopping Tomcat\"\n"\
            "                su \$TOMCAT_USER -c \$TOMCAT_PATH/bin/shutdown.sh\n"\
            "        fi\n"\
            "}\n"\
            "\n"\
            "case \"\$1\" in\n"\
            "    'start')\n"\
            "        start\n"\
            "        ;;\n"\
            "    'stop')\n"\
            "        stop\n"\
            "        ;;\n"\
            "    'restart')\n"\
            "        stop\n"\
            "        sleep 4\n"\
            "        killall java\n"\
            "        start\n"\
            "        ;;\n"\
            "    *)\n"\
            "        echo \"Usage: \$0 { start | stop | restart }\"\n"\
            "        ;;\n"\
            "esac\n"\
            "\n"\
            "exit 0\n" > /etc/init.d/tomcat

        chmod 755 /etc/init.d/tomcat
        update-rc.d tomcat defaults

        if ! service tomcat restart; then
            echo -e "${color[red]}tomcat restart failed${color[default]}"
        fi

        echo -e "tomcat install [${color[green]}done${color[default]}]"
    fi
}

install_mysql(){
    is_install mysql
    if [[ ${installCurrentApp} -eq 1 ]] ; then
        apt_get_update
        echo "installing mysql..."
        apt-get install mysql-server
        echo -e "mysql-server install [${color[green]}done${color[default]}]"
        is_config "mysql database"

        if [[ ${installCurrentApp} -eq 1 ]] ; then
            echo -e "add new user for mysql"
            echo "please insert username:"
            read usernameMysql
            echo "please insert password:"
            read passwordMysql

            echo "please insert database name:"
            read databaseMysql

            if ! echo "CREATE USER '${usernameMysql}'@'localhost' IDENTIFIED BY '${passwordMysql}';GRANT ALL PRIVILEGES ON ${databaseMysql}.* TO '${usernameMysql}'@'localhost';" | mysql ; then
                echo "CREATE USER '${usernameMysql}'@'localhost' IDENTIFIED BY '${passwordMysql}';GRANT ALL PRIVILEGES ON ${databaseMysql}.* TO '${usernameMysql}'@'localhost';" | mysql -p
            fi

            echo "please insert password for ${usernameMysql} to create database:"
            echo "CREATE DATABASE ${databaseMysql} CHARACTER SET utf8 COLLATE utf8_general_ci;" | mysql -u ${usernameMysql} -p

            echo -e "mysql-server config [${color[green]}done${color[default]}]"
        fi
    fi
}

install_apache
install_java
install_tomcat
install_mysql

echo -e "${color[yellow]}installer finish, thanks for usage ;)${color[default]}"

